Bence Nemeth - assignment

requirements:
	R
	ggplot2
	NanoStringNorm
	heatmaply

files:
	data - contains the RCC raw files
	figure - output png images of the visualisations in main.R
	main.R - R script to 3.1.2 and 3.2.1 tasks
	report.pptx - overview about the solution with inserted figures